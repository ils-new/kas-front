import fetch from '@/utils/fetch'

export function GetTopNavs() {
  return fetch({
    url: '/permission/firstMenu',
    method: 'get'
  })
}

export function GetSideNavs(pid) {
  return fetch({
    url: '/permission/subMenu',
    method: 'get',
    params: { pid }
  })
}
