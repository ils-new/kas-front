import fetch from '@/utils/fetch'
var qs = require('qs')

export function queryPage(page) {
  return fetch({
    url: '/order/queryPage',
    method: 'post',
    data: qs.stringify(page)
  })
}

export function findModifyInfo(id) {
  return fetch({
    url: '/order/findModifyInfo',
    method: 'post',
    data: qs.stringify({
      id
    })
  })
}

export function modifyInfo(data) {
  return fetch({
    url: '/order/modifyInfo',
    method: 'post',
    data: data
  })
}
// 客户名称
export function findCustomerName() {
  return fetch({
    url: '/order/getCustomer',
    method: 'get'
  })
}
// 运输方式
export function findTransportType() {
  return fetch({
    url: '/order/getTransportType',
    method: 'get'
  })
}
// 经销商
export function findDealerName() {
  return fetch({
    url: '/order/getDealerName',
    method: 'get'
  })
}
// 获取发运类型
export function findShipmentType() {
  return fetch({
    url: '/order/getShipmentType',
    method: 'get'
  })
}
// 无车承运人
export function getSupplier() {
  return fetch({
    url: '/order/getSupplier',
    method: 'get'
  })
}
// 异常查询列表
export function exceptionLineQuery(page) {
  return fetch({
    url: '/order/exception/exceptionLine',
    method: 'post',
    data: qs.stringify(page)
  })
}
// 异常分类
export function findgetClass() {
  return fetch({
    url: '/order/exception/getClass',
    method: 'get'
  })
}

export function getExcle(orderSelectBo) {
  return fetch({
    url: '/excel/getExcel',
    method: 'post',
    data: qs.stringify(orderSelectBo),
    responseType: 'blob'
  })
}
