import fetch from '@/utils/fetch'
/**
 *  列表
 * @param data
 */
export function importExperses(header, rows) {
  const data = {
    header,
    rows
  }
  return fetch({
    url: '/import/importOrder',
    method: 'post',
    data: data
  })
}

export function importOnway(header, rows) {
  const data = {
    header,
    rows
  }
  return fetch({
    url: '/import/importOnway',
    method: 'post',
    data: data
  })
}
