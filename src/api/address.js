import fetch from '@/utils/fetch'
// var qs = require('qs')
export function getCurrentPosition(address, endAddress) {
  return fetch({
    url: '/order/getLatLon',
    method: 'get',
    params: { address, endAddress }
  })
}
