import fetch from '@/utils/fetch'
var qs = require('qs')

export function login(username, password) {
  return fetch({
    url: '/account/login',
    method: 'post',
    data: qs.stringify({
      username,
      password
    })
  })
}

export function getInfo() {
  return fetch({
    url: '/account/info',
    method: 'get'
  })
}

export function logout() {
  return fetch({
    url: '/account/logout',
    method: 'post'
  })
}
