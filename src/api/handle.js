import fetch from '@/utils/fetch'

// 获取在途  /api/order/getOnWay
export function getOnWay(data) {
  return fetch({
    url: '/api/order/getOnWay',
    method: 'post',
    data: data
  })
}

//  获取订单 /api/order/getOrder
export function getOrder(data) {
  return fetch({
    url: '/api/order/getOrder',
    method: 'post',
    data: data
  })
}

//  获取单个订单 /api/order/getOrder
export function getOneOrder(data) {
  return fetch({
    url: '/api/order/pushOrderByOrderNo',
    method: 'post',
    data: data
  })
}
