import fetch from '@/utils/fetch'

/**
 *  列表
 * @param waybillCode
 */
export function orderQuery(waybillCode) {
  return fetch({
    url: '/otd/getWaybillCode',
    method: 'get',
    params: { waybillCode }
  })
}
