import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css'// Progress 进度条样式
import { Message } from 'element-ui'
import { getToken } from '@/utils/auth' // 验权
import { GetTopNavs } from '@/api/navs'
import { sortMenu, matchPath } from '@/utils/util'

const whiteList = ['/login', '/register', '/otd/wx'] // 不重定向白名单
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (getToken()) {
    if (to.path === '/login') {
      next({ path: '/' })
      // NProgress.done()
    } else {
      var isMatched = matchPath(store.getters.allrouter, to.path)
      if (store.getters.roles.length === 0) {
        store.dispatch('GetInfo').then(res => { // 拉取用户信息
          GetTopNavs().then(response => {
            store.dispatch('GetTopMenus', response.data)
            store.dispatch('InitSideNavs', sortMenu(response.data))
            store.dispatch('InitRouter').then(() => {
              // 动态添加可访问的路由
              router.addRoutes(store.getters.allrouter)
              // 判断访问的地址是否在allrouter中
              if (isMatched === true || to.path === '/dashboard') {
                next({ ...to })
              } else {
                next('/')
              }
            })
          })
          // next()
        }).catch(() => {
          store.dispatch('FedLogOut').then(() => {
            Message.error('验证失败,请重新登录')
            NProgress.done()
            next({ path: '/login' })
          })
        })
      } else {
        // 判断访问的地址是否在allrouter中
        if (isMatched === true || to.path === '/dashboard') {
          next()
        } else {
          next('/')
        }
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
