import { param2Obj } from '@/utils'

const firstMenuMap = {
  'code': 0,
  'data': [
    { 'name': '订单管理', 'orders': 1, 'id': '1' }],
  'message': 'success'
}
const subMenuMap = {
  'code': 0,
  'data': {
    'parent': {
      'id': 1,
      'systemId': 1,
      'pid': 0,
      'name': '订单管理',
      'type': 1,
      'permissionValue': '',
      'uri': null,
      'icon': '',
      'orders': 1,
      'status': 1,
      'gmtCreate': 1513661947000 },
    'children': [
      { 'name': '订单管理' },
      { 'name': '订单跟踪' },
      { 'name': '客户OTD管理' },
      { 'name': '异常查询' },
      { 'name': '手动拉取' },
      { 'name': '拉取在途' },
      { 'name': '拉取订单' }
    ]
  },
  'message': 'success' }
export default {
  firstMenu: config => {
    // const { username } = JSON.parse(config.body)
    // let temp = userMap[username]
    // console.log('firstMenu', firstMenuMap)
    return firstMenuMap
  },
  subMenu: config => {
    const { authorization } = param2Obj(config.url)
    // console.log('subMeunMap', subMenuMap)
    console.log('authorization', authorization)
    return subMenuMap
  },
  logout: () => 'success'
}
