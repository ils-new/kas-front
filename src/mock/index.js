import Mock from 'mockjs'
import loginAPI from './login'
import navAPI from './nav'

// Mock.setup({
//   timeout: '350-600'
// })

// 登录相关
// Mock.mock(/\/account\/login/, 'post', loginAPI.login)
// Mock.mock(/\/account\/logout/, 'post', loginAPI.logout)
// Mock.mock(/\/account\/info\.*/, 'get', loginAPI.getInfo)

// 获取nav相关
// Mock.mock(/\/permission\/firstMenu/, 'get', navAPI.firstMenu)
// Mock.mock(/\/permission\/subMenu/, 'get', navAPI.subMenu)

export default Mock
