export default [
  {
    path: '/order',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '订单管理',
    meta: { title: '订单管理', icon: 'b' },
    children: [{
      path: 'orderMgt',
      component: (resolve) => require(['@/views/order/orderMgt'], resolve),
      name: '订单跟踪',
      meta: { title: '订单跟踪', icon: 'table' }
    }, {
      path: 'simpleOrderMgt',
      component: (resolve) => require(['@/views/order/simpleOrderMgt'], resolve),
      name: '订单查询',
      meta: { title: '订单查询', icon: 'table' }
    }]
  },
  {
    path: '/order',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '客户OTD管理',
    meta: { title: '客户OTD管理', icon: 'b' },
    children: [{
      path: 'customerOTD',
      component: (resolve) => require(['@/views/order/customerOTD'], resolve),
      name: '客户OTD导入',
      meta: { title: '客户OTD导入', icon: 'table' }
    }, {
      path: 'unusualQuery',
      component: (resolve) => require(['@/views/order/unusualQuery'], resolve),
      name: '异常在途查询',
      meta: { title: '异常在途查询', icon: 'table' }
    }, {
      path: 'onwayImport',
      component: (resolve) => require(['@/views/order/onwayImport'], resolve),
      name: '在途轨迹导入',
      meta: { title: '在途轨迹导入', icon: 'table' }
    }]
  },
  {
    path: '/handel',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '系统维护',
    meta: { title: '系统维护', icon: 'b' },
    children: [{
      path: 'position',
      component: (resolve) => require(['@/views/handle/order'], resolve),
      name: '按客户拉取',
      meta: { title: '按客户拉取', icon: 'table' }
    },
    {
      path: 'order',
      component: (resolve) => require(['@/views/handle/position'], resolve),
      name: '拉取订单在途',
      meta: { title: '拉取订单在途', icon: 'table' }
    }, {
      path: 'oneOrder',
      component: (resolve) => require(['@/views/handle/oneOrder'], resolve),
      name: '按订单拉取',
      meta: { title: '按订单拉取', icon: 'table' }
    }]
  },
  { path: '*', name: 'error', hidden: true, redirect: '/404\'/404\'' }
]
