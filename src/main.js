import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/icons' // icon
import * as filters from './filters' // global filters
import '@/permission' // 权限
import './mock'

Vue.use(ElementUI)

Vue.config.productionTip = false
Vue.prototype.$ELEMENT = { size: 'small' }

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
