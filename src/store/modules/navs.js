
import { constantRouterMap } from '@/router'
// import { GetSideNavs, GetTopNavs } from '@/api/navs'
import { GetSideNavs } from '@/api/navs'
import path from '@/router/path'
import { GetDiffRoutes } from '@/utils/util'

const navs = {
  state: {
    routers: [],
    addRouters: [],
    navmenus: []
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
    },
    SET_NullROUTERS: (state, routers) => {
      state.addRouters = routers
    },
    SET_NavMenus: (state, menus) => {
      state.navmenus = menus
    },
    SET_ALLROUTERS: (state, routers) => {
      state.routers = routers
    }
  },
  actions: {
    // 首次进入首页时，获取侧边菜单
    InitSideNavs({ commit }, pid) {
      return new Promise(resolve => {
        GetSideNavs(pid).then(response => {
          console.log('action-response', response)
          // const naclist = [{ name: '订单管理' }, { name: '客户OTD管理' },{ name: '订单查询' },
          // { name: '订单跟踪' }, { name: '客户OTD导入' }, { name: '异常在途' },
          // { name: '手动拉取' }, { name: '拉取在途' }, { name: '拉取订单' }]
          var filterRouters = filterNavs(path, response.data) // 后台请求
          // var filterRouters = filterNavs(path, response.data.children) // mock
          // var filterRouters = filterNavs(path, naclist) // 此处自造
          console.log('最终路由', filterRouters)
          commit('SET_ROUTERS', filterRouters)
          resolve()
        })
      }).catch(error => {
        console.log(error)
      })
    },
    InitRouter({ commit }) {
      return new Promise(resolve => {
        var pid = 0
        GetSideNavs(pid).then(response => {
          console.log('InitRouter', response)
          // const naclist = [{ name: '订单' }, { name: '订单管理' }, { name: '客户OTD管理' },{ name: '订单查询' },
          // { name: '订单跟踪' }, { name: '客户OTD导入' }, { name: '异常在途' },
          // { name: '手动拉取' }, { name: '拉取在途' }, { name: '拉取订单' }]
          // var filterRouters = filterNavs(path, naclist)
          var filterRouters = filterNavs(path, response.data)
          // var filterRouters = filterNavs(path, response.data.children)
          commit('SET_ALLROUTERS', filterRouters)
          resolve()
        })
      }).catch(error => {
        console.log(error)
      })
    },
    // 点击顶部菜单时，获取侧边菜单
    GenerateSideNavsByTopNav({ commit }, pid) {
      return new Promise(resolve => {
        commit('SET_ROUTERS', path)
        resolve()
      }).catch(error => {
        console.log(error)
      })
    },
    ClearRouters({ commit }) {
      commit('SET_NullROUTERS', [])
    },
    GetTopMenus({ commit }, menus) {
      commit('SET_NavMenus', menus)
    }
  }
}

/**
 *
 * @param navs 前端维护的路由全集
 * @param perms 后端返回的可访问菜单（仅包含名称）
 */
function filterNavs(navs, perms) {
  const accessableRouters = navs.filter(route => {
    if (hasPermissions(route, perms)) {
      if (route.children && route.children.length) {
        route.children = filterNavs(route.children, perms)
      }
      return true
    }
    return false
  })
  return accessableRouters
}

/**
 *
 * @param nav 前端维护的路由（单条）
 * @param perms（后端返回的可访问菜单）
 * @returns {boolean}
 */

function hasPermissions(nav, perms) {
  if (nav && nav.name) {
    // console.log('nav', nav)
    // console.log('perms', perms)
    // console.log('falg', perms.some(perm => nav.name.indexOf(perm.name) >= 0))
    return perms.some(perm => nav.name.indexOf(perm.name) >= 0)
  } else {
    return true
  }
}

export default navs
