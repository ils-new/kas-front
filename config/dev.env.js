var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

// http://222.126.229.158:8390/kas-admin 测试环境
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://10.20.30.102:8390/kas-admin"'
})
